<?php
include("../server/app.php");
if (!usuarioActual()) {
	header("Location: ".getLink('')."components/signup.html");
	exit();
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>carrito detalle</title>
		<?php include("./static.php"); ?>
		<!-- javascript -->
		<script src="<?php echo getLink(''); ?>/app.js"></script>
		<!-- css -->
		<link href="<?php echo getLink(''); ?>/assets/css/carrito_detalle.css" rel="stylesheet"/>
  </head>
  <body>
		<div class="container m-5 text-center">
			<div class="row form">
					<div class="col-md-3">
						<div class="mb-3">
							<label for="direccion" class="form-label">Direccion de entrega</label>
							<input type="text" class="form-control" id="direccion" placeholder="Ingrese su direccion de entrega" required>
						</div>
					</div>
					<div class="col-md-3">
						<div class="mb-3">
							<label for="fechaEntrega" class="form-label">Fecha entrega</label>
							<input type="date" class="form-control" id="fechaEntrega" required>
						</div>
					</div>
					<div class="col-md-3">
						<div class="mb-3">
							<label for="horaEntrega" class="form-label">Hora Entrega</label>
							<input type="time" class="form-control" id="horaEntrega" required>
						</div>
					</div>
			</div>
			<table class='table table-bordered cart-table'>
				<thead>
					<tr>
						<th>Foto</th>
						<th>Pizza</th>
						<th>Cantidad</th>
						<th>Precio</th>
					</tr>
				</thead>
				<tbody class='cart-line-items'>
				</tbody>
				<tfoot>
					<tr>
						<td colspan='3'>Subtotal</td>
						<td class='cart-subtotal'></td>
					</tr>
				</tfoot>
			</table>
			<p class='cart-is-empty'>Tú carrito esta vacio.</p>
			<button class="btn btn-primary" onclick="confirmarPedido()">Confirmar</button>&nbsp;
			<button type="button" onclick="cancelarCarrito()" class="btn btn-danger">Cancelar</button>
		</div>
		<script>
		 $(function(){
			 Cart.initJQuery();
			 Cart.currency = 's/';
		 });
		    
		 function confirmarPedido() {
			 if ($('#direccion').val()  === "" || $('#fechaEtrega').val()  === "" || $('#horaEntrega').val() == "") {
				 alert("ingrese todos los campos")
			 } else {
				 // Enviar por AJAX
				 $.post("<?php echo getLink('components/carrito_guardar.php'); ?>",
								{
									items: Cart.items,
									fechaEntrega: $('#fechaEntrega').val(),
									horaEntrega: $('#horaEntrega').val(),
									direccion: $('#direccion').val()
								},
								function(data, status){
									Cart.empty();
									window.location.href="<?php echo getLink('components/pedido_confirmado.html');?>";
				 });
			 } 
		 }
		 function cancelarCarrito() {
			 Cart.empty();
			 window.location.href="<?php echo getLink('');?>";
		 }
		</script>
  </body>
</html>
