<?php
include("../server/app.php");
if (!(isset($_SESSION['idUsuario']) and $_SESSION['idUsuario'])) {
  header("Location: ".getLink('')."components/usuario_login.html");
  exit();
}
$db = dbConexion();
$query_pedido = mysqli_query($db, "SELECT * FROM pedido P 
INNER JOIN cliente C ON C.idCliente = P.idCliente
INNER JOIN deliveryman DM ON DM.idDeliveryMan = P.idDeliveryMan
WHERE P.idPedido = ".$_GET['idPedido']);
$query_pedido_detalle = mysqli_query($db, "SELECT * FROM pedidodetalle PD
INNER JOIN pizza P ON P.idPizza = PD.idPizza WHERE PD.idPedido =".$_GET['idPedido']);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Detalle venta</title>
		<?php include("./static.php"); ?>
  </head>
  <body>
		<div class="container m-5">
			<?php  $pedido= mysqli_fetch_object($query_pedido);?>
			<div class="row">
				<div class="col-md-4">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">
								pedido Nro <?php echo $pedido->idPedido; ?>
							</h5>
							<p>
								<b>Fecha pedido</b>
								<?php echo $pedido->fechaPedido; ?>
							</p>
							<p>
								<b>Direccion pedido</b>
								<?php echo $pedido->direccionPedido; ?>
							</p>
							<p>
								<b>Fecha Entrega</b>
								<?php echo $pedido->fechaEntrega; ?>
							</p>
							<p>
								<b>Hora entrega</b>
								<?php echo $pedido->horaEntrega; ?>
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="card bg-light">
						<div class="card-body">
							<h5 class="card-title">Cliente</h5>
							<p>
								<b>Nombre: </b>
								<?php echo $pedido->nombreCliente. " " . $pedido->apellidoCliente; ?>
							</p>
							<p>
								<b>Telefono</b>
								<?php echo $pedido->telefonoCliente; ?>
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="card bg-light">
						<div class="card-body">
							<h5 class="card-title">
								Delivery Man
							</h5>
								<p>
									<b>Delivery Man Nombre</b>
									<?php echo $pedido->deliveryManNombre; ?>
								</p>
						</div>
					</div>
				</div>
			</div>
			<table class="table table-striped">
				<thead>
					<tr>
						<th align="center">Nombre pizza</th>
						<th align="center">Cantidad</th>
						<th align="center">Precio</th>
						<th align="center">Sub total</th>
					</tr>
				</thead>
				<tbody>
					<?php
					while($pedido_detalle = mysqli_fetch_object($query_pedido_detalle)):
					?>
						<tr>
								<td align="center">
									<?php echo $pedido_detalle->nombrePizza;?>
								</td>
								<td align="center">
									<?php echo $pedido_detalle->cantidad;?>
								</td>
								<td align="center">
									<?php echo $pedido_detalle->precio;?>
								</td>
								<td align="center">
									<?php echo $pedido_detalle->precio * $pedido_detalle->cantidad;?>
								</td>
							</tr>
					<?php endwhile; ?>
				</tbody>
			</table>	
			<a href="<?php echo getLink('components/pedidos.php') ?>">Volver a pedidos</a>
		</div>
  </body>
</html>
