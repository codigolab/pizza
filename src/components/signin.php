<?php
include("../server/function_data_base.php");
$name = $_POST["name"];
$lastname = $_POST["lastname"];
$email = $_POST["email"];
$phone = $_POST["phone"];
$username = $_POST["username"];
$password = $_POST["password"];
$name_table = "cliente";

if ($name == "" || $lastname == "" || $email == "" || $phone == "" || $username == "" || $password == "") {
    header("Location: ".getLink('')."components/signin.html");
    exit();
   
} else if(isset($name, $lastname, $email, $phone, $username, $password)) {
    cliente($name_table, $name, $lastname, $email, $phone, $username, $password);
    header("Location: ".getLink('')."components/signup.html");
    exit();
}
?>
