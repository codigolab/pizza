<?php
include("../server/function_data_base.php");
$idCliente = $_SESSION["idCliente"];

$name = $_POST["name"];
$lastname = $_POST["lastname"];
$email = $_POST["email"];
$phone = $_POST["phone"];
$username = $_POST["username"];
$password = $_POST["password"];
$name_table = "cliente";

if ($name == "" || $lastname == "" || $email == "" || $phone == "" || $username == "" || $password == "") {
    header("Location: ".getLink('')."components/update_account_form.php");
    exit();
   
} else if(isset($name, $lastname, $email, $phone, $username, $password)) {
    $resultado = update_account_cliente($name_table, $name, $lastname, $email, $phone, $username, $password, $idCliente);
    if ($resultado) {
        header("Location: ".getLink(''));
        exit();
    } else {
        echo "<p>Tu cuenta no se ha podido modificar</p>";
    }
}
?>
