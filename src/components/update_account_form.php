<?php
include("../server/function_data_base.php");

$db = dbConexion(); 
$idCliente = $_SESSION["idCliente"];
$query = mysqli_query($db, "SELECT * FROM cliente where idCliente = ".$idCliente);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- bootstrap -->
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
		<!-- font awesome -->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
		<!-- css -->
		<link href="../assets/css/signin.css" rel="stylesheet"/>
    <title>Update account</title>
  </head>
  <body>
		<div class="container">
			<div class="row">
				<div class="form-login col-lg-6">
					<form action="update_account.php" method="POST" class="form-container">
						<div class="input-group form-input__container">
							<label class="input-group-text" for="name">
								<i class="fas fa-user"></i>
							</label>
							<?php
							$row = mysqli_fetch_row($query);
							print '<input type="text" name="name" id="name" placeholder="Nombres" required
											      value= '.$row[1].'
										 />
							<input type="text" name="lastname" id="lastname" placeholder="Apellidos" required
                     value= '.$row[2].'
							/>';?>

						</div>
						<div class="input-group form-input__container">
							<label class="input-group-text" for="email">
								<i class="fas fa-envelope-square"></i>
							</label>
							<?php
							print '<input type="text" name="email" id="email" placeholder="Correo electronico" required
											      value= '.$row[3].'
										 />

							<label class="input-group-text" for="phone">
								<i class="fas fa-phone"></i>
							</label>
							<input type="text" name="phone"	id="phone" pattern="[0-9]{9}"	placeholder="Numero celular" required
                     value= '.$row[4].'
							/>';
							?>
						</div>
						<div class="input-group form-input__container">
							<label class="input-group-text" for="username">
								<i class="fas fa-lock"></i>
							</label>
							<?php
							print '<input type="text" name="username" id="username" placeholder="Username" required
											      value= '.$row[5].'
										 />

							<label class="input-group-text" for="password">
								<i class="fas fa-lock"></i>
							</label>
							<input type="text" name="password" id="password" placeholder="Password"	required
                     value= '.$row[6].'
							/>';
							?>
						</div>
						<input class="form-input__send" type="submit" value="Update account"/>
						<div class="link-signin">
							<a href="./signup.html">Sign In</a>
						</div>
					</form>
				</div>
			</div>
		</div>
  </body>
</html>
