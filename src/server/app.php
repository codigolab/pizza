<?php
// iniciar sesession
session_start();
// main dir
$homeDir = 'C:\xampp\htdocs\pizza';
// URL BASE
$baseUrl = 'http://localhost/pizza/src/';

// connecion a bd
function dbConexion() {
    $db = mysqli_connect("localhost", "root", "", "pizza");
    mysqli_select_db($db, "pizza");
    return $db;
}
// obtenr link
function getLink($path) {
    global $baseUrl;
    return $baseUrl . $path;
}
// Retornar cantidad carrito
function carritoCantidad() {
    $cantidad = 0;
    // Sumar en la session
    // var_dump($_SESSION);
    foreach($_SESSION['cart'] as $item) {
        $cantidad += 1;
    }
    return $cantidad;
}
// Retornar username auth
function usuarioActual() {
    return isset($_SESSION['usernameCliente']) ? $_SESSION['usernameCliente'] : null;
}
?>
